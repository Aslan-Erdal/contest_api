import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { Contest } from "../entity/contest.entity";
import { DeleteResult, Repository, UpdateResult } from "typeorm";
import { InjectRepository } from "@nestjs/typeorm";
import { Game } from "src/game/entity/game.entity";
import { Player } from "src/player/entity/player.entity";

@Injectable()
export class ContestService {
    constructor(
        @InjectRepository(Contest)
        private readonly contestRepository: Repository<Contest>,
        @InjectRepository(Game)
        private readonly gameRepository: Repository<Game>,
        @InjectRepository(Player)
        private readonly playerRepository: Repository<Player>,
    ) { }

    public async getAllContests(): Promise<Contest[]> {
        let contest = this.contestRepository.find({ relations: { game: true, players: true, winner: true }});
        console.log("------------------");
        (await contest).map(t => console.log(t.players))
        console.log("------------------");
        return contest;
    }

    public async getOneContest(id: number): Promise<Contest> {
        return this.contestRepository.findOne({ where: { id }, relations: { game: true, players: true, winner: true}})
    }

    public async addContest(contest: Partial<any>): Promise<Contest> {
        const {game, winner, startDate, players} = contest;
        const newContest = await this.contestRepository.create(contest);
        const foundedGame = await this.gameRepository.findOne({where: { title: game}});
        const foundedWinner = await this.playerRepository.findOne({ where: { nickname: winner}});
        newContest.startDate = startDate;
        newContest.game = foundedGame;
        newContest.winner = foundedWinner ?? null;
        newContest.players = null;
        let createdContest: Contest = await this.contestRepository.save(newContest);
        return createdContest;
    }

    public async updateContest(id: number, contest: Partial<any>) {
        const contestToUpdate = await this.contestRepository.findOne({where : { id }, relations: { game: true, players: true, winner: true}});
        const {game, winner, startDate, players} = contest;
        const foundedGame = await this.gameRepository.findOne({ where: { title: game}});
        const foundedWinner = await this.playerRepository.findOne({ where: { nickname: winner}});
        const playerToAdd = await this.playerRepository.findOne({ where: { nickname: players }});
        contestToUpdate.startDate = startDate;
        contestToUpdate.game = foundedGame;
        contestToUpdate.winner = foundedWinner ?? null;
        contestToUpdate.players = [...contestToUpdate.players, playerToAdd] ?? null;
        return this.contestRepository.save(contestToUpdate);
    }

    public async deleteContest(id: number): Promise<DeleteResult> {
        const foundedContest = await this.contestRepository.findOne({ where: { id }});

        if (foundedContest) {
            return await this.contestRepository.delete(id);
        }
        throw new HttpException("Game not Found", HttpStatus.NOT_FOUND);
        
    }
}