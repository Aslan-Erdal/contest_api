import { Game } from "src/game/entity/game.entity";
import { Player } from "src/player/entity/player.entity";
import { Column, Entity, JoinColumn, ManyToMany, ManyToOne, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Contest {

    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne((_) => Game, (game) => game.contests, {
        onDelete: "CASCADE",
        onUpdate: "CASCADE",
    })
    @JoinColumn({name: "game_id"})
    game: Game;

    @ManyToOne((_) => Player, (player) => player.winners, {
        onDelete: "SET NULL",
        onUpdate: "CASCADE",
    })
    @JoinColumn({name: "player_id"})
    winner: Player | null;

    @Column({ type: 'timestamptz', name: "start_date"})
    startDate: Date | string;
    
    @ManyToMany(
        (_) => Player,
        players => players.contests,
            {
                onDelete: "CASCADE",
                onUpdate: "CASCADE"
            }
    )
    players: Player[];
    

}