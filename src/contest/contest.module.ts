import { Module } from "@nestjs/common";
import { ContestController } from "./controller/contest.controller";
import { ContestService } from "./service/contest.service";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Contest } from "./entity/contest.entity";
import { Player } from "src/player/entity/player.entity";
import { Game } from "src/game/entity/game.entity";

@Module({
    imports:[TypeOrmModule.forFeature([Contest, Player, Game])],
    controllers: [ContestController],
    providers: [ContestService],
    exports: [ContestService],
})
export class ContestModule { }