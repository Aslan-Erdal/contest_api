import { Body, Controller, Get, Param, ParseIntPipe, Post, Render, Res } from "@nestjs/common";
import { Game } from "src/game/entity/game.entity";
import { ContestService } from "../service/contest.service";
import { DeleteResult } from "typeorm";
import { Response, response } from "express";
import { Contest } from "../entity/contest.entity";

@Controller("contests")
export class ContestController {
    constructor(private readonly contestService: ContestService) { }

    @Get()
    @Render("contest/home")
    public async getContests(): Promise<Contest[]> {
        let contests = await this.contestService.getAllContests();
        contests.forEach((f) => f.startDate = f.startDate.toLocaleString());
        return { contests: contests } as unknown as Contest[];
    }

    @Get('form')
    @Render('contest/form')
    public async getContestAddForm() {
        return {};
    }

    @Get(":id")
    @Render("contest/fiche")
    public async getOneContest(
        @Param('id', ParseIntPipe) id: number,
    ): Promise<Contest> {
        let contest = await this.contestService.getOneContest(id);
        contest.startDate = contest.startDate.toLocaleString();
        return { contest: contest } as unknown as Contest;
    }

    // Update 
    @Get(':id/form')
    @Render('contest/form')
    public async getContestUpdateForm(
        @Param('id', ParseIntPipe) id: number,
    ) {
        let contest = await this.contestService.getOneContest(id);
        contest.startDate = contest.startDate.toLocaleString();
        return { contest: contest };
    }

    @Post(":id/modifier")
    public async updateGame(
        @Param('id', ParseIntPipe) id: number,
        @Body() contest: Contest,
        @Res() response: Response,
    ): Promise<void> {
        this.contestService.updateContest(id, contest);
        return response.redirect("/contests");
    }

    @Post(":id/supprimer")
    public async deleteContest(
        @Param('id', ParseIntPipe) id: number,
        @Res() response: Response,
    ): Promise<DeleteResult> {
        let deletedContest = await this.contestService.deleteContest(id);
        response.redirect("/contests");
        return deletedContest;
    }

    @Post("add")
    public async addContest(
        @Body() contest: Contest,
        @Res() response: Response,
    ): Promise<Contest> {
        response.redirect("/contests");
        return this.contestService.addContest(contest);
    }
}