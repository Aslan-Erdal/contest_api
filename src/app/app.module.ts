import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ContestModule } from 'src/contest/contest.module';
import { GameModule } from 'src/game/game.module';
import { PlayerModule } from 'src/player/player.module';
import { DataSource } from 'typeorm';
require('dotenv').config();

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      // Use useFactory, useClass, or useExisting
      // to configure the DataSourceOptions.
      useFactory: (configService: ConfigService) => ({
        type: 'postgres',
        port: 5432, //+configService.get('POSTGRES_PORT'),
        username: '<db username>',//configService.get('POSTGRES_USERNAME'),
        password: '<db password>', //configService.get('POSTGRES_PASSWORD'),
        database: 'games', //configService.get('POSTGRES_DATABASE'),
        entities: ['dist/**/*.entity{.ts,.js}'],
        autoLoadEntities: true,
        synchronize: true,
      }),
      // dataSource receives the configured DataSourceOptions
      // and returns a Promise<DataSource>.
      dataSourceFactory: async (options) => {
        const dataSource = await new DataSource(options).initialize();
        return dataSource;
      },
    }),
    PlayerModule,
    GameModule,
    ContestModule
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
