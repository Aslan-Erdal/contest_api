import { Contest } from "src/contest/entity/contest.entity";
import { Column, Entity, JoinTable, ManyToMany, OneToMany, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Player {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    email: string;

    @Column()
    nickname: string;

    @OneToMany((_) => Contest, (contests) => contests.winner,
    {
        cascade: true
    })
    winners: Contest[];

    @ManyToMany(
        () => Contest,
        contests => contests.players,
        {
            cascade: true,
        }
    )
    @JoinTable({
        name: 'player_contest',
        joinColumn: {
            name: 'player_id',
            referencedColumnName: 'id',
        },
        inverseJoinColumn: {
            name: 'contest_id',
            referencedColumnName: 'id'
        }
    })
    contests: Contest[];

}