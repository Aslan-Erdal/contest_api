import { Body, Controller, Delete, Get, Param, ParseIntPipe, Post, Put, Render, Res } from "@nestjs/common";
import { PlayerService } from "../service/player.service";
import { Player } from "../entity/player.entity";
import { DeleteResult, UpdateResult } from "typeorm";
import { Response } from "express";

@Controller('players')
export class PlayerController {

    constructor(private readonly playerService: PlayerService,) {}

    @Get()
    @Render('home')
    public async findPlayers() {
        let players = await this.playerService.findAllPlayers();
        return { players : players }
    }

    @Get('form')
    @Render('joueur/form')
    public async getPlayerAddForm() {
        return {};
    }
    
    @Get(':id')
    @Render('joueur/fiche')
    public async findPlayer(
        @Param('id', ParseIntPipe) id: number,
    ) {
        let player = await this.playerService.findPlayerById(id);
        console.log(player)

        return { player: player };
    }

    @Get(':id/supprimer')
    @Render('joueur/delplayer')
    public async getDeletePlayer(
        @Param('id', ParseIntPipe) id: number,
    ) {
        let player = await this.playerService.findPlayerById(id);
        console.log(player)

        return { player: player };
    }

    @Post(":id/supprimer")
    public async deletePlayer(
        @Param('id', ParseIntPipe) id: number,
        @Res() response: Response,
    ): Promise<void> {
        this.playerService.deletePlayer(id);
        return response.redirect("/players");
    }

    // Update 
    @Get(':id/form')
    @Render('joueur/form')
    public async getPlayerUpdateForm(
        @Param('id', ParseIntPipe) id: number,
    ) {
        let player = await this.playerService.findPlayerById(id);
        console.log(player)

        return { player: player };
    }

    @Post(":id/modifier")
    public async updatePlayer(
        @Param('id', ParseIntPipe) id: number,
        @Body() player: Player,
        @Res() response: Response,
    ): Promise<void> {
        await this.playerService.updatePlayer(id, player);
        return response.redirect("/players");
    }

    @Post("add")
    public async addPlayer(
        @Body() player: Player,
        @Res() response: Response,
    ): Promise<void> {
        await this.playerService.savePlayer(player);
        return response.redirect("/players");
    }
}