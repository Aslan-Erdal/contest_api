import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Player } from "../entity/player.entity";
import { DeleteResult, Repository, UpdateResult } from "typeorm";
import { Contest } from "src/contest/entity/contest.entity";
import { Game } from "src/game/entity/game.entity";

@Injectable()
export class PlayerService { 

    constructor(
        @InjectRepository(Player)
        private readonly playerRepository: Repository<Player>,
        // @InjectRepository(Contest)
        // private readonly contestRepository: Repository<Contest>,
        // @InjectRepository(Game)
        // private readonly gameRepository: Repository<Game>,
    ) { }

    public async findAllPlayers(): Promise<Player[]> {
       return await this.playerRepository.find({ relations: { contests: { game: true, winner: true }}});       
    }

    public async findPlayerById(id: number): Promise<Player> {
       return await this.playerRepository.findOne({ where: { id }, relations: { contests: {game: true }, winners: true }});
    }

    public async savePlayer(player: Player): Promise<Player> {
        const newPlayer = await this.playerRepository.create(player);
        newPlayer.nickname = player.nickname;
        newPlayer.email = player.email;
        let createdPlayer: Player = await this.playerRepository.save(newPlayer);
        return createdPlayer;
    }

    public async updatePlayer(id: number, player: Partial<Player>): Promise<UpdateResult> {
        const foundedPlayer: Player = await this.playerRepository.findOne({ where: { id }});
        foundedPlayer.nickname = player.nickname;
        foundedPlayer.email = player.email;
        return await this.playerRepository.update(foundedPlayer.id, foundedPlayer);
    }

    public async deletePlayer(id: number): Promise<DeleteResult> {
        let foundedPlayer = await this.playerRepository.findOne({where: { id }});
        if (!foundedPlayer) {
            throw new HttpException("Player is already Deleted or Not Found", HttpStatus.NOT_FOUND);
        }
        return this.playerRepository.delete(foundedPlayer.id);
    }
}