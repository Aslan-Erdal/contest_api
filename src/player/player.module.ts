import { Module } from "@nestjs/common";
import { PlayerController } from "./controller/player.controller";
import { PlayerService } from "./service/player.service";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Player } from "./entity/player.entity";
import { GameModule } from "src/game/game.module";
import { ContestModule } from "src/contest/contest.module";

@Module({
    imports: [TypeOrmModule.forFeature([Player]),],
    controllers: [PlayerController],
    providers: [PlayerService],
    exports: []
})
export class PlayerModule { }