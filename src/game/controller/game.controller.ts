import { Body, Controller, Get, Param, ParseIntPipe, Post, Put, Render, Res } from "@nestjs/common";
import { Game } from "../entity/game.entity";
import { GameService } from "../service/game.service";
import { DeleteResult } from "typeorm";
import { Response } from "express";

@Controller("games")
export class GameController {

    constructor(private readonly gameService: GameService) { }

    @Get()
    @Render("game/home")
    public async getGames(): Promise<Game[]> {
        let games = await this.gameService.getAllGames();
        return { games: games } as unknown as Game[];
    }

    @Get('form')
    @Render('game/form')
    public async getPlayerAddForm() {
        return {};
    }

    @Get(":id")
    @Render("game/fiche")
    public async getOneGame(
        @Param('id', ParseIntPipe) id: number,
    ): Promise<Game> {
        let game = await this.gameService.getOneGame(id);
        return { game: game } as unknown as Game;
    }

    // Update 
    @Get(':id/form')
    @Render('game/form')
    public async getGameUpdateForm(
        @Param('id', ParseIntPipe) id: number,
    ) {
        let game = await this.gameService.getOneGame(id);
        return { game: game };
    }

    @Post(":id/modifier")
    public async updateGame(
        @Param('id', ParseIntPipe) id: number,
        @Body() game: Game,
        @Res() response: Response,
    ): Promise<void> {
        this.gameService.updateGame(id, game);
        return response.redirect("/games");
    }

    @Post(":id/supprimer")
    public async deleteGame(
        @Param('id', ParseIntPipe) id: number,
        @Res() response: Response,
    ): Promise<DeleteResult> {
        let deletedUser = await this.gameService.deleteGame(id);
        response.redirect("/games");
        return deletedUser;
    }

    @Post("add")
    public async addGame(
        @Body() game: Game,
        @Res() response: Response,
    ): Promise<void> {
        this.gameService.addGame(game);
        response.redirect("/games");
    }
}