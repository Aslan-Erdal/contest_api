import { Contest } from "src/contest/entity/contest.entity";
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Game {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    title: string;

    @Column({ name: 'min_players' })
    minPlayers: number;

    @Column({ name: 'max_players' })
    maxPlayers: number;

    @OneToMany(() => Contest, (contests) => contests.game,
        // {
        //     cascade: true,
        // }
    )
    contests: Contest[];
}