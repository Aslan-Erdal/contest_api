import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { Game } from "../entity/game.entity";
import { InjectRepository } from "@nestjs/typeorm";
import { DeleteResult, Repository, UpdateResult } from "typeorm";

@Injectable()
export class GameService {

    constructor(
        @InjectRepository(Game)
        private readonly gameRepository: Repository<Game>,
    ) { }

    public async getAllGames(): Promise<Game[]> {
        return this.gameRepository.find({ relations: { contests: true }})
    }

    public async getOneGame(id: number): Promise<Game> {
        return this.gameRepository.findOne({ where: { id }, relations: { contests: true }})
    }

    public async addGame(game: Game): Promise<Game> {
        const newGame = await this.gameRepository.create(game);
        newGame.title = game.title;
        newGame.minPlayers = game.minPlayers;
        newGame.maxPlayers = game.maxPlayers;
        let createdGame: Game = await this.gameRepository.save(newGame);
        return createdGame;
    }

    public async updateGame(id: number, game: Game): Promise<UpdateResult> {
        const gameToUpdate = await this.gameRepository.findOne({where : { id }});
        gameToUpdate.title = game.title;
        gameToUpdate.minPlayers = game.minPlayers;
        gameToUpdate.maxPlayers = game.maxPlayers;
        return this.gameRepository.update(gameToUpdate.id, gameToUpdate);
    }

    public async deleteGame(id: number): Promise<DeleteResult> {
        const foundedGame = await this.gameRepository.findOne({ where: { id }});

        if (foundedGame) {
            return await this.gameRepository.delete(id);
        }
        throw new HttpException("Game not Found", HttpStatus.NOT_FOUND);
        
    }
}